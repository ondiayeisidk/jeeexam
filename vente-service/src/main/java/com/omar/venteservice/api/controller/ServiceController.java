package com.omar.venteservice.api.controller;

import com.omar.venteservice.api.entity.Service;
import com.omar.venteservice.api.service.ServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/service")
public class ServiceController {

    @Autowired
    private ServiceService serviceService;

    @GetMapping(value ="/findService")
    public List<Service> findAll(){
        return serviceService.getServices();
    }

    @PostMapping(value = "/addClasse")
    public Service save(@RequestBody Service c){
        return serviceService.saveClasse(c);
    }
}
