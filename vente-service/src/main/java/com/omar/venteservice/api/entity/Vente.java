package com.omar.venteservice.api.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "vente")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Vente {
    @Id
    @GeneratedValue
    private Long id;
    private String dateVente;
    @ManyToOne
    private Client client;

    private Long serviceId;
}
