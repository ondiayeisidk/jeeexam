package com.omar.venteservice.api.service;

import com.omar.venteservice.api.DTO.PaiementDTO;
import com.omar.venteservice.api.entity.Service;
import com.omar.venteservice.api.entity.Client;
import com.omar.venteservice.api.entity.Vente;
import com.omar.venteservice.api.repository.VenteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

@org.springframework.stereotype.Service
public class VenteService {
    @Autowired
    private VenteRepository venteRepository;
    @Autowired
    private ClientService clientService;
    @Autowired
    private ServiceService serviceService;
    @Autowired
    private RestTemplate template;

    public Vente saveInsc(Vente vente){
        Client client = clientService.saveEleve(vente.getClient());
        Optional<Service> cl = serviceService.getServiceById(vente.getServiceId());
        vente.setClient(client);
        vente.setServiceId(cl.get().getId());
        return venteRepository.save(vente);
    }

    public Vente EnregistrerVente(Vente vente){
        String response ="";
        //Vente vente = vent.getDateVente();
        Client client = clientService.saveEleve(vente.getClient());
        Optional<Service> cl = serviceService.getServiceById(vente.getServiceId());
        vente.setClient(client);
        vente.setServiceId(cl.get().getId());
        venteRepository.save(vente);

        PaiementDTO paiementDTO = new PaiementDTO();
        paiementDTO.setVenteId(vente.getId());
        paiementDTO.setMontant(20000);
        //rest call
       PaiementDTO paiementResponse = template.postForObject("http://VALIDATION-SERVICE/payment/doPaiement", paiementDTO, PaiementDTO.class);
       response = paiementResponse.getPaymentStatus().equals("succes")?"Paiement réussi!":"Paiement échoué!";

       return vente;
    }
}
