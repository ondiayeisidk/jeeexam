package com.omar.venteservice.api.service;

import com.omar.venteservice.api.entity.Service;
import com.omar.venteservice.api.repository.ServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

@org.springframework.stereotype.Service
public class ServiceService {
    @Autowired
    private ServiceRepository serviceRepository;

    public List<Service> getServices(){
        return  serviceRepository.findAll();
    }
    public Service saveClasse (Service service){
        return serviceRepository.save(service);
    }

    public Optional<Service> getServiceById(Long id){
        return serviceRepository.findById(id);
    }
}
