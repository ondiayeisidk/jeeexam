package com.omar.venteservice.api.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaiementDTO {

    private Long paymentId;
    private String paymentStatus;
    private String transactionId;
    private Long venteId;
    private int montant;
}
