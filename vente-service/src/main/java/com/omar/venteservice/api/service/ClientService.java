package com.omar.venteservice.api.service;

import com.omar.venteservice.api.entity.Client;
import com.omar.venteservice.api.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ClientService {
    @Autowired
    private ClientRepository clientRepository;

    public List<Client> getEleves(){
        return  clientRepository.findAll();
    }
    public Client saveEleve(Client client){
        return clientRepository.save(client);
    }
    public Optional<Client> getClientById(Long id){
        return clientRepository.findById(id);
    }
}
