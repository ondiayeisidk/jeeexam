package com.omar.venteservice.api.controller;


import com.omar.venteservice.api.entity.Vente;
import com.omar.venteservice.api.service.VenteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/vente")
public class VenteController {

    @Autowired
    private VenteService venteService;

    @PostMapping(value = "/save")
    public Vente save(@RequestBody Vente vente){

        return venteService.saveInsc(vente);
    }

    @PostMapping(value = "/addVente")
    public Vente saveVente(@RequestBody Vente vente){

        return venteService.EnregistrerVente(vente);
    }
}
