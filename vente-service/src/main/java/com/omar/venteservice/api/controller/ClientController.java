package com.omar.venteservice.api.controller;

import com.omar.venteservice.api.entity.Client;
import com.omar.venteservice.api.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/client")
public class ClientController {
    @Autowired
 private ClientService clientService;

    @GetMapping(value ="/findClient")
    public List<Client> findAll(){
        return clientService.getEleves();
    }

    @PostMapping(value = "/addClient")
    public Client save(@RequestBody Client e){
        return clientService.saveEleve(e);
    }
}
