package com.omar.cloud.gateway;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
public class FallBackController {

    @RequestMapping(value = "/venteFallBack")
    public Mono<String> venteServiceFallback(){
        return Mono.just("Vente Service prend du temps à répondre. Réessayer plus tard svp!");
    }

    @RequestMapping(value = "/validationFallBack")
    public Mono<String> validationServiceFallback(){
        return Mono.just("Paiement Service  prend du temps à répondre. Réessayer plus tard svp");
    }
}
