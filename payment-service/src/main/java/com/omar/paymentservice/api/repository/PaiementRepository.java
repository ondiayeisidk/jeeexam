package com.omar.paymentservice.api.repository;

import com.omar.paymentservice.api.entity.Paiement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaiementRepository extends JpaRepository<Paiement, Long> {
    Paiement findByVenteId(Long id);
}
